function solution(record) {
  let answer = [];

  for (let i = 0; i < record.length; i++) {
    const recordSplit = record[i].split(" ");
    const enterLeave = recordSplit[0];
    const uid = recordSplit[1];
    let name = "";
    if (recordSplit[2]) {
      name = recordSplit[2];
    } else {
      name = "";
    }

    if (enterLeave === "Enter") {
      const checkUser = answer.find((elem) => elem.uid === uid);
      if (checkUser) {
        answer = answer.map((elem) => {
          if (elem.uid === uid) {
            return { ...elem, user: name };
          } else {
            return elem;
          }
        });
      }
      answer.push({ uid: uid, user: name, message: "came in." });
    } else if (enterLeave === "Leave") {
      const checkUser = answer.find((elem) => elem.uid === uid);
      if (checkUser) {
        answer.push({
          uid: uid,
          user: checkUser.user,
          message: "has left.",
        });
      }
    } else {
      answer = answer.map((elem) => {
        if (elem.uid === uid) {
          return {
            ...elem,
            user: name,
          };
        }
        return elem;
      });
    }
  }
  return answer.map((elem) => `${elem.user} ${elem.message}`);
}

console.log(
  solution([
    "Enter uid1234 Spiderman",
    "Enter uid4567 Ironman",
    "Leave uid1234",
    "Enter uid1234 Ironman",
    "Change uid4567 Hulk",
  ])
);
